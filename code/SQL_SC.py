#create temp table

all_data=f"""
select * from (
select sold_price,sold_date,oak2 from avm_sales.genworth 
union
select saleprice1 as sold_price, saledate1 as sold_date, oak2 from avm_sales.bc 
union
select sold_price, sold_date, oak2 from avm_sales.barons 
union 
select price as sold_price, finalized_date as sold_date, oak2 from avm_sales.lonewolf 
union 
select sold_price, sold_at as sold_date, oak2 from avm_sales.ontario
union
select sold_price , TO_DATE(sold_date, 'YYYY-MM-DD'), oak2 from avm_sales.ontario_2
) a where sold_date<CURRENT_DATE
"""


create_temp_table=f"""
create table latest_sales_temp_1 as
(select a.oak2,max(b.sold_price) as sold_price, a.sold_date from 
(select a.oak2,max(a.sold_date) as sold_date from ({all_data}) a
group by oak2
) a
inner join
( 
{all_data}
) b on a.oak2=b.oak2 and b.sold_date=a.sold_date 
group by a.oak2,a.sold_date
);

ALTER TABLE latest_sales_temp_1 ADD PRIMARY KEY (oak2);

"""

drop_temp_table=f""" drop table latest_sales_temp_1;"""

insert_into_table=f"""
insert into avm_sales.latest_sale_1 
select * from latest_sales_temp_1 a 
where a.oak2 not in (select oak2 from avm_sales.latest_sale_1);
"""

update_table=f"""
UPDATE  avm_sales.latest_sale_1 
SET
    sold_price=latest_sales_temp_1.sold_price,
    sold_date=latest_sales_temp_1.sold_date
from latest_sales_temp_1
where  avm_sales.latest_sale_1.oak2=latest_sales_temp_1.oak2 and avm_sales.latest_sale_1.sold_date<latest_sales_temp_1.sold_date;
"""

see_if_table_exist=f"""
select * from avm_sales.latest_sale_1
limit 1;
"""
create_table=f"""
create table avm_sales.latest_sale_1 as
(select * from latest_sales_temp_1);

ALTER TABLE avm_sales.latest_sale_1 ADD PRIMARY KEY (oak2);
CREATE INDEX idx_date ON avm_sales.latest_sale_1 (sold_date);
"""