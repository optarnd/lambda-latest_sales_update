import json
import SQL_SC
import os
import psycopg2
from psycopg2.extras import RealDictCursor



# PostGres parameters
HOSTNAME = os.environ['PG_HOSTNAME']
USERNAME = os.environ['PG_USERNAME']
PASSWORD = os.environ['PG_PASSWORD']
DATABASE = os.environ['PG_DATABASE']


# Execute PostGres query
def pg_query_2(connection, query):
    try:
        cur = connection.cursor(cursor_factory=RealDictCursor)
        cur.execute(query)
        cur.close()
        connection.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        message = {
            'type' : 'Bad Request',
            'httpStatus': 400,
            'message' : 'Cannot connect to PostGres'
            }
        raise Exception(message)

def Status_400(_input):
    return {
        'statusCode': 400,
        'body': json.dumps(_input)
    }

def lambda_handler(event, context):
    conn = psycopg2.connect(host=HOSTNAME, user=USERNAME, password=PASSWORD, dbname=DATABASE)
    #Drops temp table
    try:
        pg_query_2(conn, SQL_SC.drop_temp_table)
    except:
        conn = psycopg2.connect(host=HOSTNAME, user=USERNAME, password=PASSWORD, dbname=DATABASE)
    
    #Creates new temp table
    try:
        pg_query_2(conn, SQL_SC.create_temp_table)
    except:
        Status_400("Error in creating table")
   
   #See if table exists
    try:
       pg_query_2(conn, SQL_SC.see_if_table_exist)
    except:
        conn = psycopg2.connect(host=HOSTNAME, user=USERNAME, password=PASSWORD, dbname=DATABASE)
        pg_query_2(conn, SQL_SC.create_table)
   
    #Inserts data into latest sales table
    try:
        pg_query_2(conn, SQL_SC.insert_into_table)
    except:
        Status_400("Error in insert data into table")
    
    #Updates old data in sales table
    try:
        pg_query_2(conn, SQL_SC.update_table)
    except:
        Status_400("Error in updating table")
    
    #Return status 200
    return {
        'statusCode': 200,
        'body': json.dumps('Update Complete')
    }
